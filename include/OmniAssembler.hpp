//
// Created by Diego Diaz on 3/25/18.
//

#ifndef OMNITIGSUNIBOSS_OMNITIGASSEMBLER_HPP
#define OMNITIGSUNIBOSS_OMNITIGASSEMBLER_HPP

#include <sdsl/int_vector.hpp>
#include <sdsl/bit_vectors.hpp>
#include "HOBOSSdBG.hpp"
#include "OverlapTreeTopology.hpp"
#include "easylogging++.h"
#include <sys/time.h>
#include <sys/resource.h>

class OmniAssembler {

private:
    typedef size_t size_type;
    typedef typename OverlapTreeTopology::range_type        range_type;
    typedef typename HOBOSSdBG::symbol_type                 symbol_type;

public:
    void static generateOmnitigs(HOBOSSdBG& boss_index,
                                 std::string output_prefix,
                                 size_t& min_omni_size,
                                 bool right_maximal);

private:
    static std::pair<size_t, size_t> extend_omni_path(HOBOSSdBG &dbg_index, std::pair<size_t, size_t> starter_node,
                                                          std::vector<uint8_t> &omnitig, bool right_maximal);
    static void generate_maximal_omnitigs(HOBOSSdBG &boss_index,
                                          size_t min_omni_size,
                                          bool right_maximal,
                                          std::ofstream &os,
                                          sdsl::bit_vector& visited_nodes,
                                          sdsl::rrr_vector<63>::rank_1_type& dollars_rs,
                                          std::vector<std::pair<size_t, float>>& stats);
    void static generate_singletons(HOBOSSdBG &hboss_index,
                                    size_t min_omni_size,
                                    std::ofstream &os,
                                    sdsl::bit_vector& visited_nodes,
                                    sdsl::rrr_vector<63>::rank_1_type& dollars_rs,
                                    std::vector<std::pair<size_t, float>>& stats);

    void static generate_statistics(std::string& output_prefix, std::vector<std::pair<size_type, float>>& stats);//,

    void static get_starters(HOBOSSdBG &boss_index);
};


#endif //OMNITIGSUNIBOSS_OMNITIGASSEMBLER_HPP
