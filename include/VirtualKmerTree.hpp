//
// Created by Diego Diaz on 4/5/18.
//

#ifndef OMNITIGSUNIBOSS_VIRTUAL_SUFFIX_TREE_HPP
#define OMNITIGSUNIBOSS_VIRTUAL_SUFFIX_TREE_HPP

#include <sdsl/int_vector_buffer.hpp>
#include <sdsl/int_vector.hpp>
#include <sdsl/sd_vector.hpp>

class VirtualKmerTree {

public:
    typedef sdsl::int_vector<>::size_type size_type;
private:
    typedef sdsl::sd_vector<> bit_vector_type;
    typedef sdsl::sd_vector<>::rank_1_type rank_1_type;

public:
    explicit VirtualKmerTree(sdsl::cache_config& config, size_t K);
    ~VirtualKmerTree();
    typedef std::tuple<size_type, size_type, size_type> node_type;
    std::vector<node_type> get_children(node_type node);
    bool is_dollar_valid(node_type node, size_type K);
    bool is_root(node_type node);

    inline bool is_leaf(VirtualKmerTree::node_type node) const{
        return (std::get<0>(node)==std::get<1>(node) || std::get<2>(node)==m_K);
    };

    inline bool has_child_with_dollar(node_type node){
        size_t sa_pos, depth;
        sa_pos = sa[std::get<0>(node)];
        depth = std::get<2>(node);
        return (m_rank_1_supp(sa_pos+depth+1) - m_rank_1_supp(sa_pos + depth))>0;
    };

    size_type n_leaves();
    bool ends_with_dollar(node_type node);

private:
    size_type get_depth(size_type lb, size_type rb);

private:
    size_t m_K;
    sdsl::int_vector_buffer<> sa;
    sdsl::int_vector_buffer<> lcp;
    sdsl::int_vector_buffer<> up;
    sdsl::int_vector_buffer<> down;
    sdsl::int_vector_buffer<> next_l_index;
    bit_vector_type m_dollar_bv;
    rank_1_type m_rank_1_supp;
};


#endif //OMNITIGSUNIBOSS_VIRTUAL_SUFFIX_TREE_HPP
