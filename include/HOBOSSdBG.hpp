//
// Created by Diego Diaz on 3/19/18.
//
#ifndef OMNITIGSUNIBOSS_BOSS_DBG_HPP
#define OMNITIGSUNIBOSS_BOSS_DBG_HPP

#include <iostream>
#include <fstream>
#include <bitset>

#include <sdsl/sdsl_concepts.hpp>
#include <sdsl/int_vector.hpp>
#include <sdsl/construct.hpp>
#include <sdsl/construct_lcp.hpp>
#include <sdsl/construct_sa.hpp>
#include <sdsl/construct_bwt.hpp>
#include <sdsl/wt_algorithm.hpp>
#include <sdsl/sd_vector.hpp>
#include <limits>

#include "RLEdgeBWT.hpp"
#include "OverlapTreeTopology.hpp"
#include "VirtualKmerTree.hpp"
#include "DNAAlphabet.hpp"
#include "FastXParser.hpp"
#include "easylogging++.h"

class HOBOSSdBG{

private:
    typedef rrr_vector<63>                            t_comp_bv;
    typedef typename rrr_vector<63>::rank_0_type      t_rank_0;
    typedef typename rrr_vector<63>::select_0_type    t_select_0;
    typedef typename rrr_vector<63>::select_1_type    t_select_1;

    size_t                                            m_k{};
    size_t                                            m_min_k{};
    size_t                                            m_solid_kmers=0; //# of kmers without dollar symbols

    RLEdgeBWT                                         m_edge_bwt;
    OverlapTreeTopology                               m_ovp_tree;
    t_comp_bv                                         m_node_marks;
    t_rank_0                                          m_node_marks_rs;
    t_select_0                                        m_node_marks_ss;
    t_comp_bv                                         m_dollar_pref;
    t_select_1                                        m_dollar_pref_ss;

public:
    typedef std::pair<size_t, size_t>                 range_t;
    typedef uint8_t                                   symbol_type;
    typedef uint64_t                                  size_type;
    typedef std::pair<size_type, bool>                degree_t;
    struct edge_t{
        size_type out_node;
        std::vector<uint8_t> label;
    };

    const size_t &k                     =             m_k;
    const size_t &min_k                 =             m_min_k;
    const size_t &solid_kmers           =             m_solid_kmers;

    const t_comp_bv& node_marks         =             m_node_marks;
    const RLEdgeBWT& edge_bwt           =             m_edge_bwt;
    const OverlapTreeTopology& ovp_tree =             m_ovp_tree;
    const t_comp_bv& dollar_pref        =             m_dollar_pref;

    const t_rank_0& node_marks_rs       =             m_node_marks_rs;
    const t_select_0& node_marks_ss     =             m_node_marks_ss;
    const t_select_1& dollar_pref_ss    =             m_dollar_pref_ss;

public:
    //constructors
    HOBOSSdBG();
    HOBOSSdBG(std::string& input_file, cache_config& config, const size_t & K, const size_t & min_str_depth);

    //navigational functions
    inline size_type dbg_outgoing(range_t v, uint8_t t) const {
        if(has_dollar_edges(v)) t++;
        assert(t>0 && t<=(v.second-v.first+1));
        return m_edge_bwt.LFmapping(std::get<0>(v)+ t-1);
    };//t is the rank inside the range [1..\sigma]

    inline size_type dbg_outgoing(size_type v, uint8_t t) const {
        range_t edges = get_edges(v);
        if(has_dollar_edges(edges)) t++;
        assert(t>0 && t<=(edges.second-edges.first+1));
        return m_edge_bwt.LFmapping(std::get<0>(edges)+ t-1);
    };//t is the rank inside the range [1..\sigma]


    inline size_type dbg_incomming(size_type v, uint8_t t) const{
        //TODO if the first is dollar, then move to the next as with outgoing
        assert(t>0);
        if(v==0) return 0;
        uint8_t symbol = m_edge_bwt.pos2symbol(v);
        size_t rank = v - m_edge_bwt.m_runs_acc[symbol-1];
        size_t edge_pos = m_edge_bwt.select(rank, symbol);

        if(t==1){
            return m_node_marks_rs.rank(edge_pos);
        }else{
            size_t next_edge_pos = m_edge_bwt.select(rank+1, symbol);

            size_t dollars_s = m_edge_bwt.m_dollars_rs.rank(edge_pos);
            size_t dollars_e = m_edge_bwt.m_dollars_rs.rank(next_edge_pos);

            size_t inv_s = m_edge_bwt.m_in_marks_rs.rank(edge_pos-dollars_s);
            size_t inv_e = m_edge_bwt.m_in_marks_rs.rank(next_edge_pos-dollars_e);

            size_t rank_i = m_edge_bwt.m_f_symbols.rank(inv_e, symbol);
            size_t rank_j = m_edge_bwt.m_f_symbols.rank(inv_s, symbol);

            assert(t<=(rank_j-rank_i));

            size_t symbol_pos = m_edge_bwt.m_f_symbols.select(rank_i+t-1, symbol);
            size_t n_zeroes = symbol_pos - m_edge_bwt.m_in_marks_rs(symbol_pos);
            return m_edge_bwt.m_in_marks_ss.select(n_zeroes)+1+dollars_s;
        }
    }


    inline std::pair<size_type, bool> dbg_outdegree(size_type v) const {
        range_t range;
        bool has_dollar;
        range = get_edges(v);
        has_dollar = has_dollar_edges(range);
        return std::make_pair(range.second-range.first+1, has_dollar);
    };

    std::pair<size_type, bool> dbg_indegree(size_type v)const;

    std::vector<edge_t> ovp_trans_outgoings(size_type v);
    std::vector<size_type> outgoings(size_type v);

    bool is_ovp_uniq(size_type v, std::vector<uint8_t> &label, std::vector<size_type> &tran_nodes,
                     size_type& l, size_type& k);

    inline size_type overlaping_kmer(size_type v) const {
        return ovp_tree.overlap_leaf(v);
    };

    bool unique_out(size_t v, std::vector<uint8_t> &label,
                    std::vector<size_type> &trans_nodes,
                    size_type& l, size_type& k);

    bool unique_in(size_t v, std::vector<uint8_t> &label,
                    std::vector<size_type> &trans_nodes,
                    size_type& l, size_type& k);


    size_type reverse_complement(size_type v);

    inline bool has_dollar_edges(range_t range) const{
        return (m_edge_bwt.m_dollars_rs.rank(range.second+1)-m_edge_bwt.m_dollars_rs.rank(range.first))!=0;
    }

    inline range_t get_edges(size_type v) const {
        size_t start, end;
        if(v==0){
            start=0;
        }else {
            start = m_node_marks_ss.select(v) + 1;
        }
        end = m_node_marks_ss.select(v+1);
        return std::make_pair(start, end);
    }

    //statistic functions
    inline size_type num_of_edges() const {return m_edge_bwt.size();};
    size_type num_of_nodes() const {return m_ovp_tree.n_leaves;};

    size_type num_of_dollar_edges() const{ return m_edge_bwt.m_dollars_rs(m_edge_bwt.size());}
    size_type num_of_marked_edges() const{ return m_edge_bwt.m_f_symbols.size();}

    std::vector<std::pair<size_t, size_t>> backward_search(std::vector<uint8_t> &query, uint8_t k);

    //storage functions
    size_type serialize(std::ostream& out, structure_tree_node* v, std::string name)const;
    void load(std::istream&);

    std::vector<uint8_t> get_kmer(size_type v) const {
        std::vector<uint8_t> kmer(m_k-1);
        for(size_t i=0;i<m_k-1;i++){
            if(v==0){
                kmer[i] = 1; //only dollars
            }else {
                kmer[i] = m_edge_bwt.pos2symbol(v);
            }
            size_t rank = v - m_edge_bwt.m_runs_acc[kmer[i]-1];
            v = node_marks_rs.rank(m_edge_bwt.select(rank, kmer[i]));
        }
        return kmer;
    }



    void get_prefix(size_type v, std::vector<size_type> &pn_array, size_t &size){
        uint8_t symbol;
        size=0;
        while(v!=0){
            symbol = m_edge_bwt.pos2symbol(v);
            size_t rank = v - m_edge_bwt.m_runs_acc[symbol-1];
            v = node_marks_rs.rank(m_edge_bwt.select(rank, symbol));
            pn_array[size] = v;
            size++;
        }
    }

    void print_kmer(size_type v, bool rev) const {
        // if(rev) invert the direction of the kmer
        std::vector<uint8_t> kmer = get_kmer(v);
        if(rev){
            for (unsigned long l = kmer.size(); l-- > 0;) {
                std::cout << DNAAlph::comp2char[kmer[l]];
            }
        }else{
            for (unsigned char l : kmer) {
                std::cout << DNAAlph::comp2char[l];
            }
        }
        std::cout<<""<<std::endl;
    }



private:
    void build_sa_bwt_lcp(cache_config &config);
    void build_edgebwt(cache_config &config, size_t K);
    void build_klcp(cache_config &config, size_t K);
    void build_overlap_tree(cache_config &config, size_t K, size_t min_K);
};

#endif //OMNITIGSUNIBOSS_BOSS_DBG_HPP
