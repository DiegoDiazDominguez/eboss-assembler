//
// Created by diediaz on 27-11-18.
//

#include <easylogging++.h>
#include <iostream>
#include <fstream>
#include "HOBOSSdBG.hpp"

INITIALIZE_EASYLOGGINGPP

int main(int argc, char* argv[]) {

    double t_outd=0,t_outg=0,t_ind=0,t_ovp_uniq=0, t_uniq_o=0, t_rev_comp=0;
    clock_t begin, end;
    size_t n_samp=0;

    if(argc==1){
        std::cout<<"missing dbg index"<<std::endl;
        exit(1);
    }

    HOBOSSdBG boss;
    std::string input_file(argv[1]);
    sdsl::load_from_file(boss, input_file);

    std::random_device rd; // obtain a random number from hardware
    std::mt19937 eng(rd()); // seed the generator
    std::uniform_int_distribution<size_t> distr(1, boss.solid_kmers); // define the range
    std::vector<size_t> trans_nodes(boss.k);
    std::vector<uint8_t> label(boss.k);
    size_t k,l;


    for(size_t i=0;i<1000;i++){

        size_t solid_node = boss.dollar_pref_ss(distr(eng));
        std::pair<size_t, bool> outd = boss.dbg_outdegree(solid_node);

        if((outd.first-outd.second)==0) continue;

        begin = clock();
        boss.dbg_outdegree(solid_node);
        end = clock();
        t_outd+= double(end - begin) / (CLOCKS_PER_SEC/1e6);

        begin = clock();
        boss.dbg_outgoing(solid_node, 1);
        end = clock();
        t_outg+= double(end - begin) / (CLOCKS_PER_SEC/1e6);

        begin = clock();
        boss.dbg_indegree(solid_node);
        end = clock();
        t_ind+= double(end - begin) / (CLOCKS_PER_SEC/1e6);

        begin = clock();
        boss.is_ovp_uniq(solid_node, label, trans_nodes, l, k);
        end = clock();
        t_ovp_uniq+= double(end - begin) / (CLOCKS_PER_SEC/1e6);

        begin = clock();
        boss.unique_out(solid_node, label, trans_nodes, l, k);
        end = clock();
        t_uniq_o+= double(end - begin) / (CLOCKS_PER_SEC/1e6);

        begin = clock();
        boss.reverse_complement(solid_node);
        end = clock();
        t_rev_comp+= double(end - begin) / (CLOCKS_PER_SEC/1e6);
        n_samp++;
    }

    std::cout<<"#File_name\tOutdegree(usecs)\tOutgoing(usecs)\tIndegree(usecs)\tOvp_uniq(usecs)\tUniq_out(usecs)\tRev comp(usecs)"<<std::endl;
    std::cout<<input_file<<"\t"
    <<t_outd/n_samp<<"\t"
    <<t_outg/n_samp<<"\t"
    <<t_ind/n_samp<<"\t"
    <<t_ovp_uniq/n_samp<<"\t"
    <<t_uniq_o/n_samp<<"\t"
    <<t_rev_comp/n_samp<<std::endl;

    return 0;
}

