#include <easylogging++.h>
#include <CLI11.hpp>
#include "HOBOSSdBG.hpp"
#include "OmniAssembler.hpp"

INITIALIZE_EASYLOGGINGPP

struct arguments{
    //common arguments
    std::string input_file;
    std::string tmp_dir;
    std::string id;

    //index construction arguments
    std::string output_index_prefix;
    size_t min_order{};
    size_t k{};

    //omnitigs assembly arguments
    size_t min_omni_size{};
    std::string output_fasta_prefix;
    bool right_maximal=false;
};

void setup_boss_cli(CLI::App *app, struct arguments& args){

    app->add_option("file", args.input_file,
                    "FASTA file with the input reads")->check(CLI::ExistingFile);
    app->add_option("K", args.k,
                    "K-mer size used for the assembly");
    app->add_option("-o,--output", args.output_index_prefix,
                    "Prefix for the output file")->set_default_val("dbg_index");
    app->add_option("-m,--min-order", args.min_order,
                    "Minimum order allowed for the DBG")->set_default_val("0");
    app->add_option("-t,--tmp-dir", args.tmp_dir,
                    "Directory where temporal files will be placed")->set_default_val(".");
}

void setup_assm_omni_cli(CLI::App *app, struct arguments& args){
    app->add_option("file", args.input_file,
                    "dBG index")->check(CLI::ExistingFile);
    app->add_option("-o,--output", args.output_fasta_prefix,
                    "Prefix for the output FASTA file")->set_default_val("omnitigs");
    app->add_option("-t,--tmp-dir", args.tmp_dir,
                    "Directory where temporal files will be placed")->set_default_val(".");
    app->add_option("-m,--minimum-omnitig-size", args.min_omni_size,
                    "Minimum size allowed for omnitigs")->set_default_val("90");
    app->add_flag("--right-maximal", args.right_maximal,
                  "Perform just right extension of omnitigs");
}

int main(int argc, char **argv){

    el::Configurations defaultConf;
    struct arguments args;

    CLI::App app("Variable-order dBG assembler");
    CLI::App *build = app.add_subcommand("build", "Build the variable order dBG index");
    CLI::App *assm_omni = app.add_subcommand("omni", "Assemble omnitigs");

    setup_boss_cli(build, args);
    setup_assm_omni_cli(assm_omni, args);

    CLI11_PARSE(app, argc, argv);

    if(argc==1){
        std::cout<<app.help()<<std::endl;
        exit(EXIT_FAILURE);
    }

    if(app.got_subcommand(build)){
        //TODO K cannot be 0!
        if(argc==2){
            std::cout << build->help() << std::endl;
            exit(EXIT_FAILURE);
        }

        defaultConf.setGlobally(el::ConfigurationType::Format,
                                "%datetime [HOBOSS-build] %level : %msg");
        el::Loggers::reconfigureLogger("default", defaultConf);

        LOG(INFO)<<"Building the HOBOSS index for the input reads";
        cache_config config(false, args.tmp_dir, "tmp");
        {
            TIMED_SCOPE(timerObj, "BOSS build");

            HOBOSSdBG hoboss(args.input_file, config, args.k, args.min_order);

            struct rusage r_usage;
            getrusage(RUSAGE_SELF, &r_usage);
            double memory_peak = 0;

#ifdef __APPLE__
            memory_peak = r_usage.ru_maxrss/(1024*1024);
#elif __linux__
            memory_peak = r_usage.ru_maxrss/(1024.0);
#endif
            std::ofstream os;
            double index_size = size_in_mega_bytes(hoboss);
            os.open(args.output_index_prefix+"_dbg_paper_stats");
            os<<"##min K: "+std::to_string(hoboss.min_k)+"\n";
            os<<"##max K: "+std::to_string(hoboss.k)+"\n";
            os<<"##tot nodes: "+std::to_string(hoboss.num_of_nodes())+"\n";
            os<<"##tot solid nodes: "+std::to_string(hoboss.solid_kmers)+"\n";
            os<<"##tot pref nodes: "+std::to_string(hoboss.num_of_nodes()-hoboss.solid_kmers)+"\n";
            os<<"##tot edges: "+std::to_string(hoboss.num_of_edges())+"\n";
            os<<"##dollar edges: "+std::to_string(hoboss.num_of_dollar_edges())+"\n";
            os<<"##marked edges: "+std::to_string(hoboss.num_of_marked_edges())+"\n";
            os<<"##ovp tree nodes: "+std::to_string(hoboss.ovp_tree.get_tot_nodes())+"\n";
            os<<"##ovp int tree nodes: "+std::to_string(hoboss.ovp_tree.get_int_nodes())+"\n";
            os<<"##Index size: "+std::to_string(sdsl::size_in_mega_bytes(hoboss))+" MB"+"\n";
            os<<"###Run-length EdgeBWT: "+std::to_string((size_in_mega_bytes(hoboss.edge_bwt)/index_size)*100)+"%\n";
            os<<"###Overlap tree: "+std::to_string((size_in_mega_bytes(hoboss.ovp_tree)/index_size)*100)+"%\n";
            os<<"###Node marks: "+std::to_string((size_in_mega_bytes(hoboss.node_marks)/index_size)*100)+"%\n";
            os<<"###Solid nodes marks: "+std::to_string((size_in_mega_bytes(hoboss.dollar_pref)/index_size)*100)+"%\n";
            os<<"##input_file: "+args.output_index_prefix+"\n";
            os<<"##memory_peak: "+std::to_string(memory_peak)+" MB\n";
            os<<"##elapsed_time: "+std::to_string(r_usage.ru_utime.tv_sec)+" seconds\n";
            os.close();
            store_to_file(hoboss, args.output_index_prefix+".boss");
        }
        LOG(INFO)<< "Done!";
    }else if(app.got_subcommand(assm_omni)){

        if(argc==2){
            std::cout << assm_omni->help() << std::endl;
            exit(EXIT_FAILURE);
        }

        defaultConf.setGlobally(el::ConfigurationType::Format,
                                "%datetime [omnitig-assm] %level : %msg");
        el::Loggers::reconfigureLogger("default", defaultConf);
        {
            TIMED_SCOPE(timerObj, "omnitig assembly");
            HOBOSSdBG boss;
            load_from_file(boss, args.input_file);

            std::ofstream os;
            os.open(args.output_fasta_prefix+"_paper_stats");
            os<<"##File: "+args.input_file+"\n";
            os.close();

            if(!args.right_maximal){
                LOG(INFO) << "Assembling variable-order maximal omnitigs";
            }else{
                LOG(INFO) << "Assembling variable-order right-maximal omnitigs";
            }

            OmniAssembler::generateOmnitigs(boss, args.output_fasta_prefix,
                                            args.min_omni_size, args.right_maximal);
        }
        LOG(INFO)<< "Done!";
    }
    return EXIT_SUCCESS;
}