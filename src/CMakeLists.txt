set(SOURCE_FILES main.cpp
        dfs_iterator.cpp
        DNAAlphabet.cpp
        FastXParser.cpp
        OmniAssembler.cpp
        HOBOSSdBG.cpp
        easylogging++.cc
        OverlapTreeTopology.cpp
        VirtualKmerTree.cpp
        RLEdgeBWT.cpp)

set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -DELPP_FEATURE_PERFORMANCE_TRACKING" )
add_executable(rboss-assembler ${SOURCE_FILES})

target_include_directories(rboss-assembler PUBLIC ${CMAKE_SOURCE_DIR}/include)
target_link_libraries(rboss-assembler LINK_PUBLIC sdsl divsufsort divsufsort64 z "-Wall -Wextra -Wshadow")
