//
// Created by diediaz on 04-09-18.
//

#ifndef OMNITIGSUNIBOSS_DNAKMER_HPP
#define OMNITIGSUNIBOSS_DNAKMER_HPP
#include "FixedWordArray.hpp"
#include "DNAAlphabet.hpp"

class DNAKmer: public FixedWordArray<unsigned char, uint64_t , 4>{
private:
    size_t p, k,c;
    friend class DNAKmerCounter;
public:
    uint64_t* (&W) = m_W;
public:

    DNAKmer(): FixedWordArray<unsigned char, uint64_t ,4>(),
               p(0),k(0),c(0){};

    explicit DNAKmer(size_t size): FixedWordArray<unsigned char, uint64_t,4>(size){
        p= (1ULL<<4ULL)-1;//to obtain the first cell
        k= ((m_virtual_len*m_word_width) % m_word_real_size)-m_word_width; // number of bits before the last cell
        c= (1ULL<<(k+m_word_width))-1;//to clear last element in the shift
    };

    void resize(size_t new_size) override {
        FixedWordArray::resize(new_size);
        p= (1ULL<<4)-1;//to obtain the first cell
        k= ((m_virtual_len*m_word_width) % m_word_real_size)-m_word_width; // number of bits before the last cell
        c= (1ULL<<(k+m_word_width))-1;
    }

    inline unsigned char back() const {
        return static_cast<unsigned char>(m_W[0] & p);
    }

    inline unsigned char front() const {
        return static_cast<unsigned char>((m_W[m_real_len-1] >> k) & p);
    }

    inline void leftShift(unsigned char new_symbol){
        size_t pos;
        m_W[0] >>=m_word_width;
        for(size_t i=1;i<m_real_len;i++){
            pos = (m_n_elms_word*i);
            elmWrite(pos-1, elmRead(pos));
            m_W[i] >>=m_word_width;
        }
        m_W[m_real_len-1] |= static_cast<uint64_t >(new_symbol) << k;
    }

    inline void rightShift(unsigned char new_symbol){
        size_t pos;
        m_W[m_real_len-1] <<=m_word_width;
        m_W[m_real_len-1] &= c;

        for(size_t i=(m_real_len-1);i>=1;i--){
            pos = (m_n_elms_word*i);
            elmWrite(pos, elmRead(pos-1));
            m_W[i-1] <<= m_word_width;
        }
        m_W[0] |= new_symbol;
    }

    //rc is reverse complement
    std::string getKmer(bool rc){
        std::string kmer("", m_virtual_len);
        if(!rc) {
            for (size_t i = 0; i < m_virtual_len; i++) {
                kmer[i] = DNAAlphabet::comp2char[elmRead(i)];
            }
        }else{
            for (size_t i = m_virtual_len; i-->0;) {
                kmer[i] = DNAAlphabet::comp2char[DNAAlphabet::comp2rev[elmRead(i)]];
            }
        }
        return kmer;
    };
};

#endif //OMNITIGSUNIBOSS_DNAKMER_HPP
