//
// Created by diediaz on 20-07-18.
//

#ifndef STRINGFINGERPRINTGENERATOR_UTILS_HPP
#define STRINGFINGERPRINTGENERATOR_UTILS_HPP
#include <iostream>

class Utils {

public:

    template <class T>
    static inline void hash_combine(std::size_t& seed, const T& v){
        std::hash<T> hasher;
        seed ^= hasher(v) + 0x9e3779b9 + (seed<<6U) + (seed>>2U);
    }

    static inline bool isOverflowInMul(size_t a, size_t b){
        if( (a==0) || (b==0)){
            return false;
        }
        size_t c = a*b;
        return b != c / a;
    };

    static inline bool isOverflowInDiv(__int128_t a, size_t b){
        if( (a==0) || (b==0)){
            return false;
        }
        size_t c = (size_t)a/b;
        __int128_t c_tmp = a/b;
        return c_tmp != c;
    };
};


#endif //STRINGFINGERPRINTGENERATOR_UTILS_HPP
