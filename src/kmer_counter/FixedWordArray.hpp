//
// Created by diediaz on 23-07-18.
//

#ifndef FASTQ_QUAL_COMPRESSION_FIXEDWORDARRAY_HPP
#define FASTQ_QUAL_COMPRESSION_FIXEDWORDARRAY_HPP
#include <iostream>
#include <limits>
#include <cstring>
#include <cassert>
#include <cmath>

template<class elem_type, class vector_type, size_t word_size= (size_t) std::numeric_limits<size_t>::digits>
class FixedWordArray {
protected:
    vector_type *m_W;
    const static size_t m_word_real_size = (size_t)std::numeric_limits<size_t>::digits;
    size_t m_virtual_len;
    size_t m_real_len;
    size_t m_word_width;
    size_t m_n_elms_word; //number of elements in a word
    vector_type one = 1;

protected:
    inline elem_type elmRead(size_t index) const{
        assert(index < m_virtual_len);
        size_t i,j, cell_i, cell_j;
        i = index*m_word_width;
        j = (index+1)*m_word_width-1;
        cell_i = i/m_word_real_size;
        cell_j = j/m_word_real_size;

        if(m_word_width==m_word_real_size){ //border case
            return m_W[cell_j];
        }else if(cell_i == cell_j){
            return (m_W[cell_j] >> (i & (m_word_real_size-1))) & ((one<<(j-i+1))-1);
        }else{
            return (m_W[cell_i] >> (i & (m_word_real_size-1) )  |
                    (m_W[cell_j] & ((one <<((j+1) & (m_word_real_size-1)))-1)) << (m_word_real_size - (i & (m_word_real_size-1))));
        }
    };

    inline void elmWrite(size_t index, elem_type value){
        assert(index < m_virtual_len);
        size_t i,j, cell_i, cell_j;
        i = index*m_word_width;
        j = (index+1)*m_word_width-1;
        cell_i = i/m_word_real_size;
        cell_j = j/m_word_real_size;
        auto c_value = static_cast<vector_type>(value);

        if(m_word_width == m_word_real_size){// border case
            m_W[cell_j] = c_value;
        }else if(cell_i==cell_j){
            m_W[cell_j] &= ~(((one<<(j-i+1)) -1) << (i & m_word_real_size-1));
            m_W[cell_j] |= c_value << (i & m_word_real_size-1);
        }else{
            m_W[cell_i] = (m_W[cell_i] & ((one<<(i & (m_word_real_size-1)))-1)) | (c_value << (i & (m_word_real_size-1)));
            m_W[cell_j] = (m_W[cell_j] & ~((one<<((j+1) & (m_word_real_size-1)))-1)) |
                        (c_value >> (m_word_real_size-(i & (m_word_real_size-1))));
        }
    };

    void copy(const FixedWordArray<elem_type, vector_type, word_size>& other){
        m_real_len = other.m_real_len;
        m_virtual_len = other.m_virtual_len;
        m_word_width = other.m_word_width;
        if(m_real_len != 0){
            if(m_W!=nullptr){
                delete [] m_W;
            }
            m_W = new vector_type[m_real_len];
            std::memcpy(m_W, other.m_W, m_real_len*sizeof(vector_type));
        }
    };

public:
    class Proxy{
       friend class FixedWordArray<elem_type, vector_type, word_size>;
    private:
        FixedWordArray<elem_type, vector_type, word_size> & m_fwa;
        size_t m_index;
        Proxy(FixedWordArray<elem_type, vector_type, word_size> & customArray, size_t index): m_fwa(customArray),
                                                           m_index(index){}
    public:
        inline operator elem_type()const{
            return m_fwa.elmRead(m_index);
        }

        inline Proxy& operator=(size_t value){
            m_fwa.elmWrite(m_index, value);
            return *this;
        }
    };

public:
    explicit FixedWordArray(size_t array_size){
        m_word_width = word_size;
        assert(word_size<=m_word_real_size);
        m_virtual_len = array_size;
        m_real_len = (size_t)ceil((double)(m_virtual_len*m_word_width)/m_word_real_size);
        m_W = new vector_type[m_real_len];
        m_n_elms_word = (sizeof(vector_type)*8)/m_word_width;
    };

    ~FixedWordArray(){
        delete[] m_W;
    };

    FixedWordArray(const FixedWordArray<elem_type, vector_type, word_size>& other): m_W(nullptr){
        copy(other);
    };

    FixedWordArray(){
        m_real_len = 0;
        m_virtual_len = 0;
        m_word_width = word_size;
        m_n_elms_word = (sizeof(vector_type)*8)/m_word_width;
        m_W= nullptr;
    };

    void flush(){
        if(m_W!=nullptr){
            memset(m_W, 0, m_real_len*sizeof(vector_type));
        }
    };

    virtual void resize(size_t new_size){
        m_virtual_len = new_size;
        if(new_size==0) {
            m_real_len = 0;
            if (m_W != nullptr) {
                delete[] m_W;
            }
            m_W = nullptr;
        }else {
            auto tmp = (size_t) ceil((double) (m_virtual_len * m_word_width) / m_word_real_size);
            if (tmp != m_real_len) {
                m_real_len = tmp;
                auto *tmp_ptr = (vector_type *) realloc(m_W, m_real_len * sizeof(vector_type));
                if (tmp_ptr != nullptr) {
                    if (m_W == nullptr) {
                        memset(tmp_ptr, 0, m_real_len * sizeof(vector_type));
                    }
                    m_W = tmp_ptr;
                } else {
                    std::cout << "Error trying to resize the array" << std::endl;
                    exit(EXIT_FAILURE);
                }
            }
        }
    };

    inline size_t size() const {
        return m_virtual_len;
    };

    inline size_t real_size() const {
        return m_real_len;
    };

    inline size_t elm(size_t pos) const{
        assert(pos<m_real_len);
        return m_W[pos];
    };

    void setWidth(size_t new_width){
        m_word_width = new_width;
        m_n_elms_word = (sizeof(vector_type)*8)/m_word_width;
        if(m_W!=nullptr){
            auto *tmp = new vector_type[m_virtual_len];
            for(size_t i=0;i<m_virtual_len;i++){
                tmp[i] = elmRead(i);
            }
            memset(m_W, 0, m_real_len*sizeof(vector_type));
            for(size_t i=0;i<m_virtual_len;i++){
                elmWrite(i, tmp[i]);
            }
            delete[] tmp;
        }
    };

    inline elem_type operator[](size_t index) const {
        return elmRead(index);
    }

    inline Proxy operator[](size_t index){
        return Proxy(*this, index);
    }

    inline bool operator==(const FixedWordArray<elem_type, vector_type, word_size> &other) const {
        return memcmp(m_W, other.m_W, sizeof(vector_type)*m_real_len)==0;
    }

    inline bool operator!=(const FixedWordArray<elem_type, vector_type, word_size> &other) const {
        return memcmp(m_W, other.m_W, sizeof(vector_type)*m_real_len)!=0;
    }

    inline bool compare(const FixedWordArray<elem_type, vector_type, word_size> &other, size_t pos) const{
        //pos is the zero-based position
        size_t n_bytes = ((pos+1)*word_size)/8;
        size_t s, e;

        if(memcmp(m_W, other.m_W, n_bytes)!=0){
            return false;
        }else{
            s = (n_bytes*8)/word_size;
            for(size_t i=s-1;i<=pos;i++){
                if(m_W[i]!=other.m_W[i]){
                    return false;
                }
            }
        }
        return true;
    }

    FixedWordArray<elem_type, vector_type, word_size>& operator=(const FixedWordArray<elem_type, vector_type, word_size>& other){
        copy(other);
        return *this;
    };

    size_t size_in_bytes() const{
        return sizeof(size_t)*4+
               sizeof(vector_type)*m_real_len+
               sizeof(m_W);
    };

    size_t getWordWidth() const{
        return m_word_width;
    }
};

#endif //FASTQ_QUAL_COMPRESSION_FIXEDWORDARRAY_HPP
