//
// Created by diediaz on 17-07-18.
//

#include "DNAKmerCounter.hpp"

DNAKmerCounter::DNAKmerCounter(std::string file, size_t k_size, size_t app_dollars): dfp(file, k_size, app_dollars){
    m_k_size=k_size;
}

void DNAKmerCounter::countDNAKmers() {
    size_t max=0, pos, array_size, nc_av, n_fingers, nkp_av, n_words, kmers_pool_size, kmer_n_bytes;
    sdsl::bit_vector hash_pos;
    hash_pos.resize(dfp.q);
    util::assign(hash_pos , int_vector<1>(dfp.q, 0));
    KmerInfo *kmerCounts;
    bool collision;
    size_t p=0;
    //clock_t begin = clock();
    while(!dfp.isFinished()){
        if(!hash_pos[dfp.curr_finger]) {
            hash_pos[dfp.curr_finger] = true;
        }
        if(dfp.curr_finger>max){
            max = dfp.curr_finger;
        }

        dfp.nextValidKmer();
        p++;
    }
    /*
    clock_t end = clock();
    double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
    std:cout<<elapsed_secs<<std::endl;*/

    hash_pos.resize(max+1);
    sdsl::bit_vector::rank_1_type rs(&hash_pos);

    n_fingers = rs.rank(hash_pos.size());
    nc_av=n_fingers;
    array_size = size_t(ceil(n_fingers*1.2)); //add an 10% of extra for collisions

    n_words = size_t(ceil(double(m_k_size*4)/64));
    kmers_pool_size = array_size*n_words;
    kmer_n_bytes = sizeof(uint64_t)*n_words;
    kmerCounts = new KmerInfo[array_size];
    auto *kmers_pool= new uint64_t[kmers_pool_size];
    nkp_av=0;

    size_t n_colls=0, tot=0;
    dfp.reset();

    //begin = clock();
    while(!dfp.isFinished()){
        pos = rs.rank(dfp.curr_finger+1)-1;
        KmerInfo &tmp = kmerCounts[pos];
        DNAKmer &tmpKmer = dfp.kmer2Hash();
        collision=true;

        if(tmp.freq==0){ //new kmer
            tmp.kmer = kmers_pool+nkp_av;
            memcpy(tmp.kmer, tmpKmer.W, kmer_n_bytes);
            tmp.freq=1;
            nkp_av+=n_words;
        }else if(memcmp(tmp.kmer, tmpKmer.W, kmer_n_bytes)==0){ //increment kmer freq
            tmp.freq++;
        }else{//collision
            while(tmp.next!=0){
                tmp = kmerCounts[tmp.next];
                if(memcmp(tmp.kmer, tmpKmer.W, kmer_n_bytes)==0){
                    tmp.freq++;
                    collision = false;
                    break;
                }
            }

            if(collision) {
                tmp.next = nc_av;
                tmp = kmerCounts[tmp.next];

                tmp.kmer = kmers_pool+nkp_av;
                tmp.freq = 1;
                memcpy(tmp.kmer, tmpKmer.W, kmer_n_bytes);

                nkp_av+=n_words;
                n_colls++;
                nc_av++;

                if(nc_av>=array_size){
                    exit(EXIT_FAILURE);
                }
            }
        }
        tot++;
        dfp.nextValidKmer();
    }

    //std::cout<<double(nc_av*(3.21*sizeof(uint64_t) + sizeof(uint32_t)+sizeof(size_t)))/1000000<<std::endl;

    /*end = clock();
    elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
    std::cout<<elapsed_secs<<std::endl;*/

    /*for(size_t i=0;i<nc_av;i++){
        std::cout<<kmerCounts[i].kmer.getKmer(false)<<std::endl;
    }*/
    std::cout<<nc_av<<" "<<double(n_colls)/tot<<std::endl;
    std::cout<<sizeof(uint64_t*)<<std::endl;
}
