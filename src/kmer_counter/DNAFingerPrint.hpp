//
// Created by diediaz on 12-07-18.
//

#ifndef STRINGFINGERPRINTGENERATOR_DNAFINGERPRINT_HPP
#define STRINGFINGERPRINTGENERATOR_DNAFINGERPRINT_HPP
#include <iostream>
#include "FastQReader.hpp"
#include "PrimeGenerator.hpp"
#include "ModOperations.hpp"
#include "DNAKmer.hpp"
#include "Utils.hpp"

class DNAFingerPrint {

private:
    static const size_t SIGMA=6;
    size_t m_tot_kmers;
    size_t m_n_printed_kmers;
    size_t m_k_size;
    int m_last_kmer_index{}; //index of the last symbol in the kmer
    int m_dollar_diff;
    DNAKmer m_pos_kmer;
    DNAKmer m_neg_kmer;
    FastQReader fqr;

    //values involved in the fingerprint computation
    size_t m_precom_vals[SIGMA]{};
    size_t d{};
    size_t m_q{}; // random prime numbers
    size_t m_pos_fp{}; // fingerprints of the original sequence
    size_t m_neg_fp{}; // fingerprints of the reverse complement
    size_t m_h{}; //d^(k_size-1) % q
    size_t m_inv_d{}; //inverse multiplicative of d under modulo q
    size_t m_curr_finger{};

public:
    const size_t &curr_finger = m_curr_finger;
    const size_t &q = m_q;

private:

    void precomputeValues(size_t min_range, size_t max_range) {

        size_t max_val, tmp_pow;

        bool prime_fits=false;
        while (!prime_fits) {
            m_q = PrimeGenerator::getRandomPrime(min_range, max_range); //random prime in the range [min_range, max_range]
            d = (size_t)ModOperations::findSmallestPrimitiveRoot((int)m_q); //primitive root of q
            m_h = ModOperations::mod_pow(d, m_k_size, m_q); // d^{kmer_size} % q for joining fingers
            m_inv_d = ModOperations::mod_pow(d, m_q - 2, m_q); // multiplicative inverse of d under % q
            max_val = std::max(d, m_inv_d); // max*m_q must fit a machine word
            if(!Utils::isOverflowInMul(m_q, max_val)) {
                prime_fits = true;
            }
        }

        //pre compute (symbol*sigma^{k-1})%q
        tmp_pow = ModOperations::mod_pow(d, m_k_size - 1, m_q); // d^{kmer_size-1} % q

        for (size_t j=0; j < DNAAlphabet::sigma; j++) {
            m_precom_vals[j] = (j * tmp_pow) % m_q;
        }
    }

    void initFingerPrints() {

        unsigned char pos_symbol, neg_symbol;
        size_t g, l;

        m_pos_fp = 0;
        m_neg_fp = 0;

        m_pos_kmer.resize(m_k_size);
        m_neg_kmer.resize(m_k_size);

        for(size_t i =0, j=m_k_size-1;i<m_k_size;i++,j--){
            pos_symbol = fqr.curr_symbol;
            neg_symbol = DNAAlphabet::comp2rev[pos_symbol];

            m_pos_fp = (d * m_pos_fp + pos_symbol);
            m_pos_fp %= m_q;

            m_pos_kmer[i] = pos_symbol;
            m_neg_kmer[j] = neg_symbol;
            fqr.nextSymbolInRead();
        }

        //calculate negative finger print
        for(size_t i=0;i<m_k_size;i++){
            neg_symbol = m_neg_kmer[i];
            m_neg_fp = (d * m_neg_fp + neg_symbol);
            m_neg_fp %= m_q;
        }

        //join finger values as f(A+B) where A is the finger of direct and B is the finger of reverse
        g = std::max(m_pos_fp, m_neg_fp);
        l = std::min(m_pos_fp, m_neg_fp);
        m_curr_finger = l * m_h + g;
        if(m_curr_finger >= m_q) m_curr_finger%=m_q;
        m_n_printed_kmers++;
        //std::cout<<m_pos_kmer.getKmer(false)<<" "<<m_neg_kmer.getKmer(false)<<std::endl;
    }


    inline void nextFingerPrints(){
        unsigned char pos_curr_symbol, pos_first_symbol;
        unsigned char neg_curr_symbol, neg_first_symbol;
        size_t g,l;

        //std::cout<<m_pos_kmer.getKmer(false)<<" "<<m_neg_kmer.getKmer(false)<<std::endl;

        //positive kmer
        pos_curr_symbol = fqr.curr_symbol;
        pos_first_symbol = m_pos_kmer.back();//m_pos_kmer[0];

        //std::cout<<int(pos_first_symbol)<<" "<<int(m_pos_kmer[0])<<std::endl;

        //negative kmer (reverse complement)
        neg_curr_symbol = DNAAlphabet::comp2rev[pos_curr_symbol];
        neg_first_symbol = m_neg_kmer.front();//m_neg_kmer[m_k_size-1];

        //std::cout<<int(neg_first_symbol)<<" "<<int(m_neg_kmer[m_k_size-1])<<std::endl;

        m_pos_fp = d * (m_pos_fp + m_q - m_precom_vals[pos_first_symbol]) + pos_curr_symbol;
        if (m_pos_fp >= m_q) m_pos_fp %= m_q;

        m_neg_fp = (m_neg_fp + m_q - neg_first_symbol) * m_inv_d + m_precom_vals[neg_curr_symbol];
        if (m_neg_fp >= m_q)m_neg_fp %= m_q;

        m_pos_kmer.leftShift(pos_curr_symbol);
        //m_pos_kmer[m_k_size-1] = pos_curr_symbol;

        m_neg_kmer.rightShift(neg_curr_symbol);
        //m_neg_kmer[0] = neg_curr_symbol;

        //join finger values as f(A+B) where A is the finger of direct and B is the finger of reverse
        g = std::max(m_pos_fp, m_neg_fp);
        l = std::min(m_pos_fp, m_neg_fp);
        m_curr_finger = l * m_h + g;
        if(m_curr_finger >= m_q) m_curr_finger%=m_q;
        fqr.nextSymbolInRead();
        //std::cout << m_curr_finger << " " << m_pos_kmer.getKmer(false) << " " << m_neg_kmer.getKmer(false) << std::endl;
    };

public:
    explicit DNAFingerPrint(std::string &fq_file, size_t k_size, size_t app_dollars):
            fqr(fq_file, FastQReader::FORWARD, app_dollars) {

        m_k_size = k_size;
        m_dollar_diff = (int)(m_k_size - fqr.append_dollars);
        if(fqr.append_dollars!=0){
            m_tot_kmers = (uint32_t)(fqr.n_chars - fqr.n_reads*(m_k_size-1) + (fqr.n_reads-1)*fqr.append_dollars);
        }else{
            m_tot_kmers = (fqr.curr_read_len - m_k_size+1)*fqr.n_reads;
        }
        m_n_printed_kmers = 0;

        //generate a random prime number m_q in the range [min_range, max_range], where m_q * max(d, d**-1) must fit a machine word
        auto min_range = (size_t)ceil((float)fqr.n_chars*0.7);
        auto max_range = (size_t)(10 * (2*m_k_size) * fqr.n_chars * log((2*m_k_size) * fqr.n_chars)); //minimize the pbb of collision
        precomputeValues(min_range, max_range);

        std::cout<<m_q<<std::endl;
        initFingerPrints();
    };

    void reset(){
        fqr.reset();
        m_n_printed_kmers=0;
        m_pos_kmer.flush();
        m_neg_kmer.flush();
        initFingerPrints();
    }

    inline DNAKmer& kmer2Hash(){
        if(m_pos_fp<=m_neg_fp){
            return m_pos_kmer;
        }else{
            return m_neg_kmer;
        }
    }

    inline void nextValidKmer(){
        nextFingerPrints();
        if(m_last_kmer_index==0){

            for(int j=0;j<(m_dollar_diff-1);j++){
                nextFingerPrints();
            }
        }
        m_n_printed_kmers++;
    };

    inline bool isFinished(){
        return m_n_printed_kmers>m_tot_kmers;
    };
};


#endif //STRINGFINGERPRINTGENERATOR_DNAFINGERPRINT_HPP
