//
// Created by diediaz on 17-07-18.
//

#ifndef STRINGFINGERPRINTGENERATOR_DNAKMERCOUNTER_HPP
#define STRINGFINGERPRINTGENERATOR_DNAKMERCOUNTER_HPP

#include <iostream>
#include "DNAFingerPrint.hpp"
#include "Utils.hpp"
#include "sdsl/bit_vectors.hpp"

class DNAKmerCounter {

private:
    struct KmerInfo{
        uint64_t *kmer=nullptr;
        uint32_t freq=0;
        size_t next=0;
    };

    DNAFingerPrint dfp;
    size_t m_k_size;

public:
    DNAKmerCounter(std::string file, size_t k_size, size_t app_dollars);
    void countDNAKmers();
};


#endif //STRINGFINGERPRINTGENERATOR_DNAKMERCOUNTER_HPP
