//
// Created by Diego Diaz on 3/26/18.
//

#include "OverlapTreeTopology.hpp"

using namespace sdsl;
using namespace std;

OverlapTreeTopology::OverlapTreeTopology() {
    m_n_leaves = 0;
}

OverlapTreeTopology::OverlapTreeTopology(const OverlapTreeTopology &ts) {
    m_n_leaves = 0;
    copy(ts);
}

OverlapTreeTopology::OverlapTreeTopology(OverlapTreeTopology::t_bit_vector &shape) {
    //create tree shape structures
    m_shape.swap(shape);
    util::assign(m_bp_supp, t_bp_support(&m_shape));
    util::init_support(m_bp_rank_10, &m_shape);
    util::init_support(m_bp_select_10, &m_shape);
    m_n_leaves = m_bp_rank_10(m_shape.size());
}

OverlapTreeTopology::size_type OverlapTreeTopology::get_tot_nodes() const {
    return m_bp_supp.rank(m_shape.size()-1);
}

OverlapTreeTopology::size_type OverlapTreeTopology::get_n_leaves() const {
    return m_n_leaves;
}

OverlapTreeTopology::size_type OverlapTreeTopology::get_int_nodes() const {
    return get_tot_nodes() - m_n_leaves;
}

void OverlapTreeTopology::print_shape() const {
    char tmp;
    for (unsigned long i : m_shape) {
        tmp = i ? '(' : ')';
        cout<<tmp<<" ";
    }
    cout<<""<<endl;
}

OverlapTreeTopology::node_type OverlapTreeTopology::range_to_id(OverlapTreeTopology::range_type& range) const {

    if(!is_range_valid(range)){
        throw range_error("tree range is not valid");
    }
    // least common ancestor
    return lca(select_leaf(get<0>(range)+1),
               select_leaf(get<1>(range)+1));
}

OverlapTreeTopology::range_type OverlapTreeTopology::id_to_range(OverlapTreeTopology::node_type id) const {
    // check if ID is a valid range
    if(!is_id_valid(id)){
        throw invalid_argument("node ID is not valid");
    }
    return make_pair(lb(id), rb(id));
}

//child range is zero based
OverlapTreeTopology::range_type OverlapTreeTopology::get_parent_range(OverlapTreeTopology::range_type& child_range) const {

    if(!is_range_valid(child_range)){
        throw range_error("tree range is not valid");
    }

    // get the ID of range
    // (I could call range_to_id but I didn't want to do double check on the range!)
    node_type child_id = lca(select_leaf(get<0>(child_range)+1),
                             select_leaf(get<1>(child_range)+1));

    if(child_id==root()){
        throw invalid_argument("asking for the parent of the root");
    }
    // return the leaves of the parenthesis enclosing the input node
    return id_to_range(m_bp_supp.enclose(child_id));
}

OverlapTreeTopology::range_type OverlapTreeTopology::contains(OverlapTreeTopology::range_type& range)const{
    node_type lca_node = lca(select_leaf(get<0>(range)+1),
                            select_leaf(get<1>(range)+1));

    // get the range of least common ancestor
    return id_to_range(lca_node);
}

bool OverlapTreeTopology::is_leaf(OverlapTreeTopology::node_type& v) const {
    assert(m_shape[v]==1);  // assert that v is a valid node of the suffix tree
    // if there is a closing parenthesis at position v+1, the node is a leaf
    return !m_shape[v+1];
}

OverlapTreeTopology::size_type OverlapTreeTopology::degree(OverlapTreeTopology::range_type& range) const {
    if(!is_range_valid(range)){
        throw range_error("tree range is not valid");
    }

    node_type v = range_to_id(range);
    size_type res = 0;
    v = v+1;
    while (m_shape[v]) { // found open parentheses
        ++res;
        v = m_bp_supp.find_close(v)+1;
    }
    return res;
}

OverlapTreeTopology::node_type OverlapTreeTopology::root() const {
    return 0;
}

vector<OverlapTreeTopology::range_type> OverlapTreeTopology::children(OverlapTreeTopology::range_type& v) const {
    if(!is_range_valid(v)){
        throw range_error("tree range is not valid");
    }

    vector<range_type> children;
    size_type parent_id = range_to_id(v);
    size_type tmp_child = parent_id+1;
    while(true){
        if(!m_shape[tmp_child]){
            break;
        }else {
            children.push_back(id_to_range(tmp_child));
            tmp_child = m_bp_supp.find_close(tmp_child) + 1;
        }
    }
    return children;
}

OverlapTreeTopology &OverlapTreeTopology::operator=(const OverlapTreeTopology &ts_new) {
    // check for self-assignment
    if(&ts_new == this)
    {
        return *this;
    }else{
        copy(ts_new);
    }
    return *this;
}

void OverlapTreeTopology::swap(OverlapTreeTopology &ts_new) {
    m_shape.swap(ts_new.m_shape);
    util::swap_support(m_bp_supp, ts_new.m_bp_supp, &m_shape, &(ts_new.m_shape));
    util::swap_support(m_bp_rank_10, ts_new.m_bp_rank_10, &m_shape, &(ts_new.m_shape));
    util::swap_support(m_bp_select_10, ts_new.m_bp_select_10, &m_shape, &(ts_new.m_shape));
    m_n_leaves = ts_new.m_n_leaves;
}

OverlapTreeTopology::size_type OverlapTreeTopology::serialize(ostream &out, structure_tree_node *v, string name) const {
    structure_tree_node* child = structure_tree::add_child(v, name, util::class_name(*this));
    size_type written_bytes = 0;
    written_bytes += m_shape.serialize(out, child, "shape");
    written_bytes += m_bp_supp.serialize(out, child, "bp_supp");
    written_bytes += m_bp_rank_10.serialize(out, child, "bp_rank_10");
    written_bytes += m_bp_select_10.serialize(out, child, "bp_select_10");

    structure_tree::add_size(child, written_bytes);
    return written_bytes;
}

void OverlapTreeTopology::load(istream &in) {
    m_shape.load(in);
    m_bp_supp.load(in, &m_shape);
    m_bp_rank_10.load(in, &m_shape);
    m_bp_select_10.load(in, &m_shape);
    m_n_leaves = m_bp_rank_10(m_shape.size()-1);
}

DfsIterator OverlapTreeTopology::begin() const {
    return {&m_shape, 2, get_tot_nodes(), &m_bp_supp};
}

DfsIterator OverlapTreeTopology::end() const {
    return {&m_shape, 1, 1, &m_bp_supp};
}

OverlapTreeTopology::node_type OverlapTreeTopology::select_leaf(OverlapTreeTopology::size_type i) const {
    assert(i > 0 and i <= m_n_leaves);
    // -1 as select(i) returns the position of the 0 of pattern 10
    return m_bp_select_10.select(i)-1;
}

OverlapTreeTopology::node_type OverlapTreeTopology::leftmost_leaf(const OverlapTreeTopology::node_type& v) const {
    return m_bp_select_10(m_bp_rank_10(v)+1)-1;
}

OverlapTreeTopology::node_type OverlapTreeTopology::rightmost_leaf(const OverlapTreeTopology::node_type& v) const {
    size_type r = m_bp_supp.find_close(v);
    return m_bp_select_10(m_bp_rank_10(r+1))-1;
}

//zero-based values
OverlapTreeTopology::size_type OverlapTreeTopology::lb(const OverlapTreeTopology::node_type& v) const {
    return m_bp_rank_10(v);
}

//also zero-based values
OverlapTreeTopology::size_type OverlapTreeTopology::rb(const OverlapTreeTopology::node_type& v) const {
    size_type r = m_bp_supp.find_close(v);
    return m_bp_rank_10(r+1)-1;
}

OverlapTreeTopology::node_type OverlapTreeTopology::lca(OverlapTreeTopology::node_type v, OverlapTreeTopology::node_type w) const {
    assert(m_shape[v] == 1 and m_shape[w] == 1);
    if (v > w) {
        std::swap(v,w);
    } else if (v==w) {
        return v;
    }
    if (v == root())
        return root();
    return m_bp_supp.double_enclose(v, w);
}

OverlapTreeTopology::node_type OverlapTreeTopology::sibling(OverlapTreeTopology::node_type& v) const {
    if (v==root())
        return root();
    node_type sib = m_bp_supp.find_close(v)+1;
    if (m_shape[sib])
        return sib;
    else
        return root();
}

bool OverlapTreeTopology::is_range_valid(OverlapTreeTopology::range_type& range) const {
    size_type sp = get<0>(range);
    size_type ep = get<1>(range);

    if(sp > ep){
        return false;
    }
    if( sp>=m_n_leaves || ep >= m_n_leaves){
        return false;
    }
    return true;
}

bool OverlapTreeTopology::is_id_valid(OverlapTreeTopology::node_type& v) const {
    if(v>=m_shape.size()){
        return false;
    }else if(!m_shape[v]){
        return false;
    }
    return true;
}

void OverlapTreeTopology::copy(const OverlapTreeTopology &ts) {
    m_n_leaves         = ts.m_n_leaves;
    m_shape            = ts.m_shape;
    m_bp_supp          = ts.m_bp_supp;
    m_bp_supp.set_vector(&m_shape);
    m_bp_rank_10       = ts.m_bp_rank_10;
    m_bp_rank_10.set_vector(&m_shape);
    m_bp_select_10     = ts.m_bp_select_10;
    m_bp_select_10.set_vector(&m_shape);
}

OverlapTreeTopology::node_type OverlapTreeTopology::contains(OverlapTreeTopology::node_type v, OverlapTreeTopology::node_type w) {
    if(!is_id_valid(v) || !is_id_valid(w)){
        throw std::invalid_argument("node ids are not valid");
    }
    // get the range of least common ancestor
    return lca(v, w);
}

OverlapTreeTopology::size_type OverlapTreeTopology::degree(OverlapTreeTopology::node_type &v) const {
    if(!is_id_valid(v)){
        throw range_error("tree range is not valid");
    }

    size_type res = 0;
    v = v+1;
    while (m_shape[v]) { // found open parentheses
        ++res;
        v = m_bp_supp.find_close(v)+1;
    }
    return res;
}

vector<OverlapTreeTopology::node_type> OverlapTreeTopology::children(OverlapTreeTopology::node_type &v) const {
    if(!is_id_valid(v)){
        throw range_error("tree range is not valid");
    }

    vector<node_type> children;
    size_type tmp_child = v+1;
    while(true){
        if(!m_shape[tmp_child]){
            break;
        }else {
            children.push_back(tmp_child);
            tmp_child = m_bp_supp.find_close(tmp_child) + 1;
        }
    }
    return children;
}

bool OverlapTreeTopology::is_root(OverlapTreeTopology::range_type &v) const {
    return v.first == 0 && v.second == (m_n_leaves - 1);
}

bool OverlapTreeTopology::is_leaf(OverlapTreeTopology::range_type &v) const {
    return v.first==v.second;
}

OverlapTreeTopology::size_type OverlapTreeTopology::overlap_leaf(OverlapTreeTopology::size_type leaf) const {
    assert(leaf<n_leaves);
    if(leaf==0) return 0;
    range_type leaf_range = {leaf, leaf};
    node_type leaf_node = range_to_id(leaf_range);
    node_type least_sibling = m_bp_supp.enclose(leaf_node)+1;
    assert(m_shape[least_sibling]); //least brother must exists
    if(least_sibling==leaf_node){
        least_sibling = m_bp_supp.enclose(least_sibling-1)+1;
    }

    return lb(least_sibling);
}

