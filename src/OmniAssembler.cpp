//
// Created by Diego Diaz on 3/25/18.
//

#include <OmniAssembler.hpp>
#include "OmniAssembler.hpp"

using namespace std::chrono;
using namespace std;
using namespace sdsl;

void OmniAssembler::generateOmnitigs(HOBOSSdBG& boss_index, std::string output_prefix,
                                     size_t& min_omni_size, bool right_maximal) {

    struct rusage r_usage{};
    double index_size = size_in_mega_bytes(boss_index);
    LOG(INFO) << "DBG has "+to_string(boss_index.num_of_nodes())+" K nodes and "+
                 to_string(boss_index.num_of_edges())+" edges";
    LOG(INFO) << "Maximum size for the kmers is "+to_string(boss_index.k);
    LOG(INFO) << "Minimum size for the kmers is "+to_string(boss_index.min_k);
    LOG(INFO) << "The size of the BOSS index is "+to_string(index_size)+" MB";
    LOG(INFO)<<"  Run-length EdgeBWT: "+to_string((size_in_mega_bytes(boss_index.edge_bwt)/index_size)*100)+"%";
    LOG(INFO)<<"  Overlap tree: "+to_string((size_in_mega_bytes(boss_index.ovp_tree)/index_size)*100)+"%";
    LOG(INFO)<<"  Node marks: "+to_string((size_in_mega_bytes(boss_index.node_marks)/index_size)*100)+"%";
    LOG(INFO)<<"  Solid nodes marks: "+to_string((size_in_mega_bytes(boss_index.dollar_pref)/index_size)*100)+"%";


    ofstream os;
    std::vector<std::pair<size_t, float>> stats;

    sdsl::bit_vector visited_nodes;
    util::assign(visited_nodes, int_vector<1>(boss_index.dollar_pref.size(), 0));
    sdsl::rrr_vector<63>::rank_1_type dollars_pref_rs;
    dollars_pref_rs.set_vector(&boss_index.dollar_pref);

    os.open(output_prefix+"_omnitigs.fasta");

    clock_t begin = clock();
    get_starters(boss_index);
    //generate_maximal_omnitigs(boss_index, min_omni_size, right_maximal, os, visited_nodes, dollars_pref_rs, stats);
    //generate_singletons(boss_index, min_omni_size, os, visited_nodes, dollars_pref_rs, stats);
    get_starters(boss_index);
    clock_t end = clock();
    os.close();

    getrusage(RUSAGE_SELF, &r_usage);
    double memory_peak = 0;
    double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;

    LOG(INFO) <<"Generating statistics";
    //generate_statistics(output_prefix, stats);

    //for the paper
    os.open(output_prefix+"_paper_stats", std::ios_base::app);
#ifdef __APPLE__
    memory_peak = double(r_usage.ru_maxrss)/(1024*1024);
#elif __linux__
    memory_peak = double(r_usage.ru_maxrss)/(1024);
#endif
    os<< "##time for searching omnitigs: "+to_string(elapsed_secs)+" seconds\n";
    os<< "##memory peak :"+to_string(memory_peak)+" MB\n";
    os.close();
    //
}


pair<size_t, size_t> OmniAssembler::extend_omni_path(HOBOSSdBG &dbg_index, std::pair<size_t, size_t> starter_node,
                                                     std::vector<uint8_t> &final_omnitig, bool right_maximal) {

    uint64_t tmp_node, omni_pos=0, extensions;
    std::pair<size_t, size_t> entry_points;
    std::map<uint64_t,bool> tv_nodes;
    std::vector<uint8_t> edge_label(dbg_index.k);
    std::vector<HOBOSSdBG::size_type> trans_node(dbg_index.k);
    HOBOSSdBG::size_type l,k;

    std::vector<std::vector<uint8_t>> omnitigs(2);

    std::vector<uint8_t> kmer_seq = dbg_index.get_kmer(starter_node.first);

    //if right_maximal is true, there will be just right extensions
    extensions = 2UL - right_maximal;

    for(size_t i=0;i<extensions;i++) {

        if(i==0){
            tmp_node = starter_node.first; //right extension
        }else{
            tmp_node = starter_node.second; //left extension
        }

        while(tmp_node!=0 && tv_nodes.count(tmp_node)==0 && dbg_index.unique_out(tmp_node, edge_label, trans_node, l, k)){

            tv_nodes[tmp_node] = true;

            //hash visited solid kmers
            for(size_t j=0;j<(k-1);j++){
                tv_nodes[trans_node[j]] = true;
            }

            //continue from the last
            tmp_node = trans_node[k-1];

            //add the symbols
            for (size_t j = 0; j < l; j++) {
                omnitigs[i].push_back(edge_label[j]);
            }
            //this is in case of a loop
            l=0;
        }

        //put the symbols of the last edge
        for (size_t j=0;j<l;j++) {
            omnitigs[i].push_back(edge_label[j]);
        }

        if(i==0){
            entry_points.first = tmp_node;
        }else{
            entry_points.second = dbg_index.reverse_complement(tmp_node);
        }
    }

    //build the final omnitig
    final_omnitig.resize(omnitigs[0].size()+omnitigs[1].size()+kmer_seq.size());

    //left extension
    if(!omnitigs[1].empty()) {
        for (size_t i = omnitigs[1].size(); i-->0;) {
            final_omnitig[omni_pos] = DNAAlph::comp2rev[omnitigs[1][i]];
            omni_pos++;
        }
    }

    //label of the starter node
    for (size_t i = kmer_seq.size(); i --> 0 ;){
       final_omnitig[omni_pos] = kmer_seq[i];
       omni_pos++;
    }

    //right extension
    if(!omnitigs[0].empty()) {
        for (unsigned char i : omnitigs[0]) {
            final_omnitig[omni_pos] = i;
            omni_pos++;
        }
    }
    return entry_points;
}

void OmniAssembler::generate_statistics(string & output_prefix, vector<pair<size_t, float>>& stats){
    size_type n_chars=0;

    sort(stats.begin(), stats.end());

    std::ofstream os( output_prefix+"_assembly_stats", ofstream::out) ;
    for (auto &stat : stats) {
        n_chars+= stat.first;
    }

    os <<"##Number of assembled omnitigs: "+to_string(stats.size())+"\n";
    os <<"##Total characters: "+to_string(n_chars)+"\n";
    os <<"##Maximum omnitig size: "+to_string(stats[stats.size()-1].first)+"\n";
    os <<"##Mean omnitig size: "+to_string(floor((float)n_chars/(float)stats.size()))+"\n";
    os <<"#Omnitig size\tGC content\n";

    for (auto &stat : stats) {
        os << to_string(stat.first)+"\t"+to_string(stat.second)+"\n";
    }
    os.close();
}

void OmniAssembler::generate_maximal_omnitigs(HOBOSSdBG &boss_index,
                                              size_t min_omni_size,
                                              bool right_maximal,
                                              std::ofstream &os,
                                              sdsl::bit_vector& visited_nodes,
                                              sdsl::rrr_vector<63>::rank_1_type& dollars_rs,
                                              std::vector<std::pair<size_t, float>>& stats){

    LOG(INFO) << "Assembling maximal omnitigs";

    HOBOSSdBG::degree_t outd;
    size_type next_kmer, first_kmer, first_rv_kmer, solid_kmer, n_paths=0;
    std::map<std::pair<size_t, size_t>, bool> paths;
    bool uniq_in;
    std::vector<uint8_t> label_in(boss_index.k), label_out(boss_index.k);
    HOBOSSdBG::size_type l_in_len, l_out_len, k_in_len, k_out_len;
    std::vector<HOBOSSdBG::size_type> trans_nodes_in(boss_index.k), trans_nodes_out(boss_index.k);

    for(size_t i=1;i<=boss_index.solid_kmers;i++){

        solid_kmer = boss_index.dollar_pref_ss(i);
        std::vector<uint8_t> edge_label;

        if((i % (boss_index.solid_kmers/10))==0){
            std::cout<<(double(i)/boss_index.solid_kmers)*100<<" completed"<<std::endl;
        }

        if(visited_nodes[i-1]==true) continue;
        //outd = boss_index.dbg_outdegree(solid_kmer);

        std::vector<size_type> outs = boss_index.outgoings(solid_kmer);
        if(outs.size()>1){

            /*std::vector<HOBOSSdBG::edge_t> outgoings = boss_index.ovp_trans_outgoings(solid_kmer);
            for (auto &outgoing : outgoings) {
                for(size_t k=0;k<(outgoing.label.size());k++){
                    std::cout<<" ";
                }
                boss_index.print_kmer(outgoing.out_node, true);
            }
            boss_index.print_kmer(solid_kmer, true);
            std::cout<<" "<<std::endl;*/



            /*HOBOSSdBG::degree_t tmp_outd = boss_index.dbg_outdegree(trans_nodes_out[k_out_len-1]);
            std::cout << tmp_outd.first<<" "<<tmp_outd.second<<std::endl;
            std::cout << solid_kmer << std::endl;
            std::cout << l_out_len << std::endl;
            size_t padding=l_out_len;
            if(trans_nodes_out[k_out_len-1]==solid_kmer) padding=0;
            if((tmp_outd.first-tmp_outd.second)<=1){
                std::vector<HOBOSSdBG::edge_t> outgoings = boss_index.ovp_trans_outgoings(trans_nodes_out[k_out_len - 1]);
                for (auto &outgoing : outgoings) {
                    for(size_t k=0;k<(outgoing.label.size()+padding);k++){
                        std::cout<<" ";
                    }
                    boss_index.print_kmer(outgoing.out_node, true);
                }
            }

            for(size_t j=0;j<padding;j++){
                std::cout<<" ";
            }
            boss_index.print_kmer(trans_nodes_out[k_out_len-1], true);
            boss_index.print_kmer(solid_kmer, true);
            std::cout<<""<<std::endl;
            for(size_t m=0;m<outs.size();m++){
                boss_index.print_kmer(outs[m], true);
            }
            std::cout << "" << std::endl;*/


            //mark visited nodes
            //visited_nodes[dollars_rs(boss_index.reverse_complement(trans_nodes_out[k_out_len-1]))] = true;
            for (unsigned long out : outs) {
                visited_nodes[dollars_rs(out)] = true;
                //visited_nodes[dollars_rs(boss_index.reverse_complement(out))] = true;
            }
            visited_nodes[i-1] = true;
            //visited_nodes[dollars_rs(boss_index.reverse_complement(solid_kmer))] = true;

            /*
            for(uint8_t k = 1;k<=(outd.first-outd.second);k++){

                next_kmer = boss_index.dbg_outgoing(solid_kmer, k);
                first_kmer = next_kmer;

                while((uniq_in = boss_index.unique_in(next_kmer, label_in, trans_nodes_in, l_in_len, k_in_len)) &&
                       boss_index.unique_out(next_kmer, label_out, trans_nodes_out, l_out_len, k_out_len)){

                    //hash visited kmers
                    for(size_t j=0;j<k_out_len;j++){
                        visited_nodes[dollars_rs(trans_nodes_out[j])] = true;
                    }

                    //no overlap and no outgoing edge
                    if(trans_nodes_out[k_out_len-1]==0) break;

                    next_kmer = trans_nodes_out[k_out_len-1];
                }

                //hash visited kmers
                for (size_t j = 0; j < k_out_len; j++) {
                    visited_nodes[dollars_rs(trans_nodes_out[j])] = true;
                }

                if(!uniq_in || boss_index.overlaping_kmer(next_kmer)==0){

                    first_rv_kmer = boss_index.reverse_complement(first_kmer);

                    std::pair<size_t, size_t> starter = {first_kmer, first_rv_kmer};
                    std::vector<uint8_t> maximal_omnitig;


                    extend_omni_path(boss_index, starter,
                                     maximal_omnitig, right_maximal);


                    if(maximal_omnitig.size()>=min_omni_size){
                        std::ostringstream omni_str;
                        stats.push_back({maximal_omnitig.size(), 0});
                        os<<">omnitig_mo"+std::to_string(first_kmer)+"\n";
                        for (unsigned char j : maximal_omnitig) {
                            omni_str.put(DNAAlph::comp2char[j]);
                        }
                        omni_str.put('\n');
                        os << omni_str.str();
                        n_paths++;
                    }
                    visited_nodes[dollars_rs(first_kmer)] = true;
                    visited_nodes[dollars_rs(first_rv_kmer)] = true;
                }
            }*/
        }
    }
    LOG(INFO) << to_string(n_paths)+" maximal omnitigs assembled";
}

void OmniAssembler::generate_singletons(HOBOSSdBG &hboss_index,
                                        size_t min_omni_size,
                                        std::ofstream &os,
                                        sdsl::bit_vector& visited_nodes,
                                        sdsl::rrr_vector<63>::rank_1_type& dollars_rs,
                                        vector<pair<OmniAssembler::size_type, float>> &stats) {

    HOBOSSdBG::degree_t solid_outd, solid_ind;
    std::map<std::pair<size_t, size_t>, bool> paths;
    bool uniq_in;
    std::vector<uint8_t> label_in(hboss_index.k), label_out(hboss_index.k);
    HOBOSSdBG::size_type solid_kmer, next_kmer, rv_next_kmer, rv_solid_kmer, n_paths=0;
    HOBOSSdBG::size_type l_in_len, l_out_len, k_in_len, k_out_len;
    std::vector<HOBOSSdBG::size_type> trans_nodes_in(hboss_index.k), trans_nodes_out(hboss_index.k);

    LOG(INFO) << "Assembling singleton paths";

    //TODO testing
    /*
    std::string tmp = "AGCTTTTCATTCTGACTGCAACGGGCAATATGTCTCTGTGTGGATTAAAAAAAGAGTGTCTGATAGCAGCTTCTGAACTGGTTACCTGCCGTGAGTAAATTAAAATTTTATTGACTTAG";
    std::vector<uint8_t> tmp_vec;
    for (auto i = tmp.size() ; i-- > 0 ; ){
        tmp_vec.push_back(DNAAlph::char2comp[tmp[i]]);
    }
    std::cout<<hboss_index.backward_search(tmp_vec,0)[0].first<<std::endl;
     */
    //

    for(size_t i=1;i<=hboss_index.solid_kmers;i++){

        solid_kmer = hboss_index.dollar_pref_ss(i);

        //TODO testing
        //solid_kmer = hboss_index.reverse_complement(66523978);
        //hboss_index.print_kmer(solid_kmer, true);
        //

        if(visited_nodes[i-1]==true) continue;

        solid_outd = hboss_index.dbg_outdegree(solid_kmer);
        solid_ind = hboss_index.dbg_indegree(solid_kmer);

        if((solid_outd.first-solid_outd.second)==0 &&
           //hboss_index.overlaping_kmer(solid_kmer)==0 &&
           solid_ind.first<=2){

            if(solid_ind.first==2 && !solid_ind.second) break;


            std::vector<uint8_t> omni_str;

            rv_solid_kmer = hboss_index.reverse_complement(solid_kmer);

            std::vector<uint8_t> kmer_seq = hboss_index.get_kmer(rv_solid_kmer);
            for (size_t l = kmer_seq.size(); l --> 0 ;){
                omni_str.push_back(DNAAlph::comp2char[kmer_seq[l]]);
            }

            next_kmer = hboss_index.dbg_outgoing(rv_solid_kmer, 1);

            omni_str.push_back(DNAAlph::comp2char[hboss_index.edge_bwt.pos2symbol(next_kmer)]);

            while((uniq_in=hboss_index.unique_in(next_kmer, label_in, trans_nodes_in, l_in_len, k_in_len)) &&
                   hboss_index.unique_out(next_kmer, label_out, trans_nodes_out, l_out_len, k_out_len)){

                //hash visited kmers
                /*for(size_t j=0;j<k_out_len;j++){
                    visited_nodes[dollars_rs(trans_nodes_out[j])] = true;
                }*/

                for (size_t l = 0; l<l_out_len;l++){
                    omni_str.push_back(DNAAlph::comp2char[label_out[l]]);
                }

                //no way out!
                if(trans_nodes_out[k_out_len-1]==0) break;

                next_kmer = trans_nodes_out[k_out_len-1];
            }

            for (size_t l = 0; l<l_out_len;l++){
                omni_str.push_back(DNAAlph::comp2char[label_out[l]]);
            }

            //if(uniq_in && k_out_len==1 && trans_nodes_out[k_out_len-1]==0){

                std::pair<size_t, size_t> entry_points = {rv_solid_kmer, next_kmer};
                std::pair<size_t, size_t> rv_entry_points = {hboss_index.reverse_complement(entry_points.second),
                                                             hboss_index.reverse_complement(entry_points.first)};

                if(omni_str.size()>=min_omni_size && paths.count(rv_entry_points)==0){
                    std::ostringstream omni_stream;
                    stats.push_back({omni_str.size(), 0});
                    os<<">omnitig_sp"+std::to_string(rv_solid_kmer)+"\n";
                    for (unsigned char j : omni_str) {
                        omni_stream.put(j);
                    }
                    omni_stream.put('\n');
                    os << omni_stream.str();
                    paths[entry_points] = true;
                    n_paths++;
                }
                visited_nodes[dollars_rs(rv_solid_kmer)] = true;
            //}
        }
    }
    paths.clear();
    LOG(INFO) << std::to_string(n_paths)+" singleton paths were assembled";
}

void OmniAssembler::get_starters(HOBOSSdBG &dbg_index) {

    std::stack<std::tuple<size_type, bool, size_type>> nodes;
    size_type solid_kmer, pref_node, depth;
    std::tuple<size_type, bool, size_type> tmp_node;
    std::vector<size_type> children;
    bool is_safe;

    //root
    tmp_node = {0, true, 0};

    children = dbg_index.ovp_tree.children(std::get<0>(tmp_node));

    std::vector<uint8_t> edge_symbols(dbg_index.k*4);
    std::vector<size_type> pref_nodes(dbg_index.k*4);
    size_t symbols_counts[]={0,0,0,0,0,0};
    size_t n_symbols=0;

    std::vector<size_type> pref_backward(dbg_index.k, 0);
    size_type pref_size=0;

    for(size_t i=children.size();i-->0;){
        if(!dbg_index.ovp_tree.is_leaf(children[i])) {
            nodes.push({children[i], true,  0});
        }
    }

    while(!nodes.empty()){
        tmp_node = nodes.top();
        nodes.pop();
        children = dbg_index.ovp_tree.children(std::get<0>(tmp_node));
        depth = std::get<2>(tmp_node);

        //dbg_index.print_kmer(dbg_index.ovp_tree.id_to_range(std::get<0>(tmp_node)).first, true);

        if(children.empty()){//A leaf was reached! (not prefix kmer)

            solid_kmer = dbg_index.ovp_tree.id_to_range(std::get<0>(tmp_node)).first;

            /*std::cout<<solid_kmer<<" "<<std::get<2>(tmp_node)<<std::endl;
            HOBOSSdBG::range_t tmp_edges = dbg_index.get_edges(solid_kmer);
            for(size_t i=tmp_edges.first;i<=tmp_edges.second;i++){
                std::cout<<DNAAlph::comp2char[dbg_index.edge_bwt[i]]<<" ";
            }
            std::cout<<" "<<std::endl;
            dbg_index.print_kmer(solid_kmer, true);*/

            if(!std::get<1>(tmp_node)){//overlapping are not safe

                //count the number of symbols
                n_symbols=0;
                memset(symbols_counts,0, sizeof(size_t)*6);
                for(size_t i=0;i<depth;i++){
                    if(symbols_counts[edge_symbols[i]]==0){
                        n_symbols++;
                    }
                    symbols_counts[edge_symbols[i]]++;
                }

                size_type last_unsafe = pref_nodes[0];
                size_type pos=0;

                while(n_symbols>1 && pos<depth){

                    symbols_counts[edge_symbols[pos]]--;
                    if(symbols_counts[edge_symbols[pos]]==0){
                        n_symbols--;
                    }

                    if(n_symbols==1){
                        last_unsafe = pref_nodes[pos];
                    }
                    pos++;
                }

                //dbg_index.print_kmer(solid_kmer, true);
                //dbg_index.print_kmer(dbg_index.dbg_incomming(solid_kmer, 1), true);
                //std::cout<<dbg_index.dbg_indegree(solid_kmer).first<<std::endl;
                //std::cout<<""<<std::endl;
                dbg_index.get_prefix(last_unsafe, pref_backward, pref_size);
                size_type tmp, tmp_node_tree;
                HOBOSSdBG::range_t tmp_range;
                tmp = last_unsafe;
                while(pref_size>=dbg_index.min_k){
                    //dbg_index.print_kmer(tmp, true);
                    tmp = dbg_index.dbg_incomming(tmp, 1);
                    tmp_range = {tmp, tmp};
                    tmp_node_tree = dbg_index.ovp_tree.bp_supp.enclose(dbg_index.ovp_tree.range_to_id(tmp_range));
                    if(tmp_node_tree!=0) {
                        tmp_range = dbg_index.ovp_tree.id_to_range(tmp_node_tree);

                        for (size_t i = tmp_range.first; i <= tmp_range.second; i++) {
                            if (dbg_index.dollar_pref[i]) {
                                if (dbg_index.has_dollar_edges(dbg_index.get_edges(i))) {//not safe
                                };
                            }
                        }
                    }
                    pref_size--;
                }

                //dbg_index.print_kmer(last_unsafe, true);
                //dbg_index.print_kmer(solid_kmer, true);
                //std::cout << pref_size << " " << last_unsafe << " " << solid_kmer<< std::endl;

            }else{
                HOBOSSdBG::range_t edges = dbg_index.get_edges(solid_kmer);
                bool has_dollars = dbg_index.has_dollar_edges(edges);
                size_type eff_edges = edges.second - edges.first + 1 - has_dollars;

                if(eff_edges==0){//outdegree 0: check if the overlapping kmer has only one dbg_incomming
                    size_type parent_node = dbg_index.ovp_tree.bp_supp.enclose(std::get<0>(tmp_node));
                    if(dbg_index.ovp_tree.children(parent_node).size()>2){ //not safe
                    }
                }

                if(eff_edges>1 ||
                   (eff_edges==1 && dbg_index.edge_bwt[edges.first+has_dollars]!=edge_symbols[depth-1])){
                    //solid is not safe
                }
            }

        }else{ //internal node

            is_safe = std::get<1>(tmp_node);

            //children[0] is always a leaf that represent a prefix kmer
            pref_node = dbg_index.ovp_tree.id_to_range(children[0]).first;

            /*std::cout<<pref_node<<" "<<std::get<2>(tmp_node)<<std::endl;
            HOBOSSdBG::range_t tmp_edges = dbg_index.get_edges(pref_node);
            for(size_t i=tmp_edges.first;i<=tmp_edges.second;i++){
                std::cout<<DNAAlph::comp2char[dbg_index.edge_bwt[i]]<<" ";
            }
            std::cout<<" "<<std::endl;
            dbg_index.print_kmer(pref_node, true);*/

            //pref_nodes can't have dollars
            HOBOSSdBG::range_t edges = dbg_index.get_edges(pref_node);
            size_t eff_edges = (edges.second-edges.first+1);
            size_t tv_edges = depth;

            for(size_t i=edges.first;i<=edges.second;i++){
                edge_symbols[tv_edges] = dbg_index.edge_bwt[i];
                pref_nodes[tv_edges] = pref_node;
                tv_edges++;
            }

            if(eff_edges>1 || (depth!=0 && edge_symbols[tv_edges-1]!=edge_symbols[tv_edges-2])){
                is_safe=false;
            }

            for (size_t i = children.size(); i-- > 1;) {
                nodes.push({children[i], is_safe, tv_edges});
            }
        }
    }
}

