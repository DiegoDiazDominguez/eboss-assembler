# rBOSS assembler 

This is a very early version of a genome assembler that works on top of the rBOSS data structure,
described [here](https://arxiv.org/abs/1901.10453). There is no document explaining the ideas
behind the assembler yet, but there will be one soon. 

##Requisites
1. SDSL-lite library
2. Cmake >=3.7

##Installation

You have to modify the CMakeLists.txt file in the following line: 

```
set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -I/path/to/include -L/path/to/lib -DELPP_FEATURE_PERFORMANCE_TRACKING -msse4.2 -O3 -funroll-loops -fomit-frame-pointer -ffast-math -DNDEBUG" )
``` 

And replace the include and lib paths for the header and lib
files for the SDSL library respectively. Then proceed as usual:

```
$ git git clone https://DiegoDiazDominguez@bitbucket.org/DiegoDiazDominguez/eboss-assembler.git 
$ cd eboss-assembler 
$ mkdir build && cd build
$ cmake ..
$ make
```

The binary files will be placed in the bin folder of the home directory of the project

##Testing the assembler:
Assuming the input file is **my_fastq.fq**

### Build the index
```
./rboss-assembler build -m min_ovp my_fastq.fq k -o output_prefix
```
Where min_ovp and k are the minimum overlap allowed between the vo-dBG nodes and k is the maximum order of the vo-dBG

### Assembling omnitigs from the index
```
./rboss-assember omni output_prefix.boss   
```

The process will generate a fasta file with the assembled omnitigs

