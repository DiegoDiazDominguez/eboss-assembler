//
// Created by Diego Diaz on 3/15/18.
//
#include "gtest/gtest.h"
#include "tree_shape.hpp"

using namespace std;
/*
class TreeShapeTest : public ::testing::Test {

public:
    tree_shape<> ts;
    tree_shape<>::range_type range;
    tree_shape<>::size_type node_id;
    tree_shape<>::size_type degree;
    vector<tree_shape<>::range_type> children;

    TreeShapeTest(): ts("/Users/diegodiaz/CLionProjects/OmnitigsUniBOSS/cmake-build-debug/lcp_just_a_name.sdsl",10,1),
                     node_id(0),
                     degree(0){}
};


TEST_F(TreeShapeTest, test_n_leaves){
    EXPECT_EQ(ts.n_leaves, 13);
}

TEST_F(TreeShapeTest, test_id_to_range){

    //range of the root
    range = ts.id_to_range(0);
    EXPECT_EQ(get<0>(range), 0);
    EXPECT_EQ(get<1>(range), ts.n_leaves - 1); //zero-based ranges

    //range of the first leaf
    range = ts.id_to_range(1);
    EXPECT_EQ(get<0>(range), 0);
    EXPECT_EQ(get<1>(range), 0);

    //range of the last leaf
    range = ts.id_to_range(32);
    EXPECT_EQ(get<0>(range), ts.n_leaves-1);
    EXPECT_EQ(get<1>(range), ts.n_leaves-1);

    //range of an internal node
    range = ts.id_to_range(5);
    EXPECT_EQ(get<0>(range), 2);
    EXPECT_EQ(get<1>(range), 6);

    //range of a invalid node (return the range of the root)
    EXPECT_THROW(ts.id_to_range(7), std::invalid_argument);
}

TEST_F(TreeShapeTest, test_range_to_id){

    //id of the root node
    node_id = ts.range_to_id(make_pair(0, ts.n_leaves-1));
    EXPECT_EQ(node_id, 0);

    //id of the fist leaf
    node_id = ts.range_to_id(make_pair(0, 0));
    EXPECT_EQ(node_id, 1);

    //id of the last leaf
    node_id = ts.range_to_id(make_pair(ts.n_leaves-1, ts.n_leaves-1));
    EXPECT_EQ(node_id, 32);

    //id of an internal node
    node_id = ts.range_to_id(make_pair(2, 6));
    EXPECT_EQ(node_id, 5);

    //id of a invalid range (return the range of the root)
    EXPECT_THROW(ts.range_to_id(make_pair(10,1)), std::range_error);
}

TEST_F(TreeShapeTest, test_get_parent_range){
    // get the parent range of the root node (the root itself)
    EXPECT_THROW(ts.get_parent_range(make_pair(0, ts.n_leaves-1)),
                 std::invalid_argument);

    // get the parent range of an invalid range (the root range)
    EXPECT_THROW(ts.get_parent_range(make_pair(14, 1)),
                 std::range_error);

    // get the parent range of the first leaf
    range = ts.get_parent_range(make_pair(0, 0));
    EXPECT_EQ(get<0>(range), 0);
    EXPECT_EQ(get<1>(range), ts.n_leaves-1);


    // get the parent range of the last leaf
    range = ts.get_parent_range(make_pair(ts.n_leaves-1, ts.n_leaves-1));
    EXPECT_EQ(get<0>(range), ts.n_leaves-2);
    EXPECT_EQ(get<1>(range), ts.n_leaves-1);


    // get the parent of an internal node
    range = ts.get_parent_range(make_pair(3, 4));
    EXPECT_EQ(get<0>(range), 2);
    EXPECT_EQ(get<1>(range), 6);


    //get the grandparent of a node
    range = ts.get_parent_range(ts.get_parent_range(make_pair(3, 3)));
    EXPECT_EQ(get<0>(range), 2);
    EXPECT_EQ(get<1>(range), 6);
}

TEST_F(TreeShapeTest, test_degree){
    // get the degree of the root
    degree = ts.degree(make_pair(0, ts.n_leaves-1));
    EXPECT_EQ(degree, 7);

    // get the degree of first leaf
    degree = ts.degree(make_pair(0, 0));
    EXPECT_EQ(degree, 0);

    // get the degree of last leaf
    degree = ts.degree(make_pair(ts.n_leaves-1, ts.n_leaves-1));
    EXPECT_EQ(degree, 0);

    // get the degree of an internal node
    degree = ts.degree(make_pair(2, 6));
    EXPECT_EQ(degree, 4);

    // get the degree of the least common ancestor of any range of nodes
    // get the degree of an internal node
    degree = ts.degree(make_pair(6, 7));
    EXPECT_EQ(degree, 7);

}

TEST_F(TreeShapeTest, test_children) {
    //get the children of the root
    //expected
    vector<tree_shape<>::range_type> res_1 = {make_pair(0,0),
                                              make_pair(1,1),
                                              make_pair(2,6),
                                              make_pair(7,8),
                                              make_pair(9,9),
                                              make_pair(10,10),
                                              make_pair(11,12)};
    children = ts.children(make_pair(0, ts.n_leaves-1));
    ASSERT_EQ(res_1, children);

    //get the children of a leaf
    vector<tree_shape<>::range_type> res_2 = {};
    children = ts.children(make_pair(0, 0));
    ASSERT_EQ(res_2, children);

    //get the children of an internal node
    vector<tree_shape<>::range_type> res_3 = {make_pair(7,7),
                                              make_pair(8,8)};
    children = ts.children(make_pair(7, 8));
    ASSERT_EQ(res_3, children);

    //get the children of an internal node
    vector<tree_shape<>::range_type> res_4 = {make_pair(2,2),
                                              make_pair(3,4),
                                              make_pair(5,5),
                                              make_pair(6,6)};
    children = ts.children(make_pair(2, 6));
    ASSERT_EQ(res_4, children);

}
 */

