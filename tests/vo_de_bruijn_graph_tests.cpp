//
// Created by Diego Diaz on 3/19/18.
//

#include "gtest/gtest.h"
/*
class VariableOrderDBGSuitCase : public ::testing::Test {
public:
    vo_dbg<> vo_dbg1;

    VariableOrderDBGSuitCase(){
        size_t min_kmer_size=0;
        bool skip_rev_comp=false;
        string input_dsk="/Users/diegodiaz/CLionProjects/OmnitigsUniBOSS/test/very_small_test.solid_kmers_binary";
        int kmer_program=1;
        string output_prefix="test_vo_dbg";

        //read kmer information
        kmer_set *ks = kmer_parser::parse_kmers(input_dsk, skip_rev_comp, kmer_program);
        //create de bruijn information
        pack_data(skip_rev_comp, output_prefix, ks);

        ifstream input(output_prefix+".packed", ios::in|ios::binary|ios::ate);
        // Can add this to save a couple seconds off traversal - not really worth it.
        //vector<size_t> minus_positions;
        debruijn_graph<> dbg = debruijn_graph<>::load_from_packed_edges(input, "$ACGT");
        input.close();

        //this is meanwhile I figure out how to build the BOSS representation efficiently
        string lcs_file = output_prefix+".lcs";
        wt_int<rrr_vector<63>> lcs;
        int_vector<> lcs_vector;
        construct(lcs, lcs_file, 1);
        lcs_vector.resize(lcs.size());

        for(size_t i=0;i<lcs.size();i++){
            lcs_vector[i] = lcs[i];
        }
        store_to_file(lcs_vector, output_prefix+".int.lcs");

        tree_shape<> ts = tree_shape<>(output_prefix+".int.lcs");
        vo_dbg1 = vo_dbg<>(dbg, ts);

        remove(output_prefix+".packed");
        remove(output_prefix+".lcs");
        remove(output_prefix+".int.lcs");

    }
};

TEST_F(VariableOrderDBGSuitCase, test_first_test){
        EXPECT_EQ(vo_dbg1.num_edges(), 10);
}

 */
